#include "host_communicator.h"

#include <Windows.h>

#include <string>
#include <vector>

CRITICAL_SECTION g_cs;
std::vector<std::string> io_read_message_queue;
std::vector<std::string> io_write_message_queue;

HANDLE io_read_thread;
HANDLE io_write_thread;

HANDLE io_read_finished;
HANDLE io_messages_present_event;
HANDLE io_write_trigger_event;

unsigned long io_write_terminate_requested;

DWORD CALLBACK io_read_proc(void* param) {
	HANDLE in_handle = GetStdHandle(STD_INPUT_HANDLE);
	std::vector<char> input_buffer(1<<20);
	std::vector<std::string> new_messages;
	int num_valid_bytes = 0;
	while (1) {
		DWORD num_read = 0;
		BOOL read_success = ReadFile(in_handle, input_buffer.data() + num_valid_bytes, (DWORD)input_buffer.size() - num_valid_bytes, &num_read, nullptr);
		if (!read_success) {
			SetEvent(io_read_finished);
			return 0;
		}
		int next_start = 0;
		int end = num_valid_bytes + num_read;
		for (int offset = num_valid_bytes; offset < end; ++offset) {
			if (input_buffer[offset] == '\n') {
				DWORD num_written = 0;
				std::string msg(input_buffer.data() + next_start, input_buffer.data() + offset + 1);
				new_messages.push_back(msg);
				OutputDebugStringA(msg.c_str());
				if (offset == next_start) {
					return 0;
				}
				next_start = offset + 1;
			}
		}
		if (next_start != 0) {
			num_valid_bytes = end - next_start;
			memmove(input_buffer.data(), input_buffer.data() + next_start, num_valid_bytes);
		}
		if (!new_messages.empty()) {
			EnterCriticalSection(&g_cs);
			io_read_message_queue.insert(io_read_message_queue.end(), new_messages.begin(), new_messages.end());
			SetEvent(io_messages_present_event);
			new_messages.clear();
			LeaveCriticalSection(&g_cs);
		}
	}
	return 0;
}

DWORD CALLBACK io_write_proc(void* param) {
	HANDLE out_handle = GetStdHandle(STD_OUTPUT_HANDLE);
	std::vector<std::string> due_messages;
	while (1) {
		WaitForSingleObject(io_write_trigger_event, INFINITE);
		EnterCriticalSection(&g_cs);
		if (io_write_terminate_requested) {
			LeaveCriticalSection(&g_cs);
			break;
		}
		LeaveCriticalSection(&g_cs);
		due_messages.swap(io_write_message_queue);
		int n = (int)due_messages.size();
		for (int i = 0; i < n; ++i) {
			auto& msg = due_messages[i];
			DWORD num_written = 0;
			WriteFile(out_handle, msg.data(), (DWORD)msg.size(), &num_written, nullptr);
		}
		due_messages.clear();
	}
	return 0;
}

static bool g_is_hosted;

void setup_communicator(bool is_hosted) {
	g_is_hosted = is_hosted;
	InitializeCriticalSection(&g_cs);
	if (is_hosted) {
		io_read_finished = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		io_messages_present_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		io_write_trigger_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		io_read_thread = CreateThread(nullptr, 0, &io_read_proc, nullptr, 0, 0);
		io_write_thread = CreateThread(nullptr, 0, &io_write_proc, nullptr, 0, 0);
	}
}

void teardown_communicator() {
	DeleteCriticalSection(&g_cs);
}

bool pump_communicator(message_process_callback cb) {
	if (g_is_hosted) {
		HANDLE hs[] = { io_read_finished, io_messages_present_event };
		DWORD wait_res = WaitForMultipleObjects(_countof(hs), hs, FALSE, 0);
		if (wait_res == WAIT_OBJECT_0) {
			return false;
		}
		else if (wait_res == WAIT_OBJECT_0 + 1) {
			std::vector<std::string> new_messages;
			EnterCriticalSection(&g_cs);
			new_messages.swap(io_read_message_queue);
			LeaveCriticalSection(&g_cs);
			cb(new_messages.data(), (int)new_messages.size());
			new_messages.clear();
		}
	}
	return true;
}
