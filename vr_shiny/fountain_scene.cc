#include "scene.h"
#include "d3d_helpers.h"
#include "vertex_mesh.h"

extern vertex_mesh fountain_particle_mesh;
extern shader_set g_solid_shader;

static float3 random_direction_close_to(LCG& lcg, float3 unit_vector, float param) {
	while (1) {
		float3 ret = float3::RandomDir(lcg);
		if (Dot(ret, unit_vector) > param) {
			return ret;
		}
	}
}

struct fountain_scene : scene {
	static flavor g_get_flavor() { return k_foreground_scene; }
	
	static GUID g_get_guid() {
		// {32DF6048-4969-4C1F-A245-BB4A007A6576}
		static GUID const s_guid = {
			0x32df6048, 0x4969, 0x4c1f, { 0xa2, 0x45, 0xbb, 0x4a, 0x0, 0x7a, 0x65, 0x76 }
		};
		return s_guid;
	}

	static std::string g_get_name() { return "Fountain"; }

	fountain_scene(ID3D11Device* dev, ID3D11DeviceContext* ctx)
		: dev(dev)
		, ctx(ctx)
	{}

	ID3D11Device* dev;
	ID3D11DeviceContext* ctx;

	struct particle_spatial {
		float3 pos;
		float3 vel;
		Quat orientation;
	};

	struct particle_meta {
		float birth_time;
		float life_duration;
		bool alive;
	};

	std::vector<particle_spatial> particle_spatials;
	std::vector<particle_meta> particle_metas;

	void activate(double now) override {
		particle_spatials.clear();
		particle_metas.clear();
		spawn_rate = 300.0;
		last_spawn = now;
		lcg = LCG(9001u);
	}
	void deactivate() override {}

	double spawn_rate;
	double last_spawn;

	LCG lcg;

	void update(double now) override {
		double dt = now - last_spawn;
		int n = (int)particle_metas.size();
		int dead_count = 0;
		for (int i = 0; i < n; ++i) {
			auto& meta = particle_metas[i];
			if (meta.alive && now - meta.birth_time >= meta.life_duration) {
				meta.alive = false;
			}
			if (!meta.alive) {
				++dead_count;
			}
		}
		int insert_slot = 0;
		double spawn_interval = 1.0f / spawn_rate;
		double spawn_count_raw = dt / spawn_interval;
		int to_spawn = (int)spawn_count_raw;
		if (to_spawn > dead_count) {
			particle_metas.resize(n + to_spawn - dead_count);
			particle_spatials.resize(n + to_spawn - dead_count);
		}
		n = (int)particle_metas.size();
		for (int i = 0; i < to_spawn; ++i) {
			particle_meta meta = {
				(float)last_spawn,
				3.0f,
				true,
			};
			float3 vel = 3.0f * random_direction_close_to(lcg, +float3::unitY, 0.9f);
			particle_spatial spatial = {
				float3::zero,
				vel,
				Quat::identity,
			};
			for (bool inserted = false; !inserted; ++insert_slot) {
				if (!particle_metas[insert_slot].alive) {
					particle_metas[insert_slot] = meta;
					particle_spatials[insert_slot] = spatial;
					inserted = true;
				}
			}
		}
		for (int i = 0; i < n; ++i) {
			if (particle_metas[i].alive) {
				auto& spatial = particle_spatials[i];
				spatial.vel += float3(0.0f, -9.82f, 0.0f) * (float)dt;
				spatial.pos += spatial.vel * (float)dt;
				if (spatial.pos.y < 0.0f) {
					spatial.vel.y = Abs(spatial.vel.y * 0.9f);
					spatial.pos.y = 0.0f;
				}
			}
		}
		last_spawn += to_spawn * spawn_interval;
	}

	void draw(scene_info const& si, int eye) override {
		struct vs_matrices {
			float4x4 proj;
			float4x4 view;
		};
		
		vs_matrices mats = {};
		mats.proj = si.projs[eye].Transposed();
		float4x4 view = float4x4(si.eyes[eye].Inverted() * si.view);
		mats.view = view.Transposed();
		ID3D11Buffer* matrix_cb = make_constant_buffer(dev, &mats, sizeof(mats));

		struct light {
			float3 dir_pos; uint32_t is_point;
			float3 color; float _pad1;
		};

		light sun = {
			view.TransformDir(-float3::unitY), 0,
			float3(0.1f, 0.1f, 0.1f), 0.0f,
		};

		std::vector<light> lights(1);
		lights[0] = sun;
		LCG lcg(9001u);
		for (int i = 0; i < 8; ++i) {
			float3 color = float3::RandomSphere(lcg, float3::zero, 0.5f).Abs();
			light lgt = {
				view.TransformPos(float3x3::RotateY(2.0f * pi * i / 8.0f).Transform(+float3::unitX)), 1,
				color, 0.0f,
			};
			lights.push_back(lgt);
		}
		ID3D11ShaderResourceView* light_srv = make_structured_buffer(dev, lights.data(), (int)lights.size(), sizeof(light));

		// Common setup
		ctx->VSSetConstantBuffers(0, 1, &matrix_cb);
		ctx->PSSetShaderResources(0, 1, &light_srv);

		std::vector<float4x4> instances;
		instances.reserve(particle_spatials.size());
		for (auto I = particle_spatials.begin(); I != particle_spatials.end(); ++I) {
			auto& particle = *I;
			float4x4 world = float4x4::Translate(particle.pos);
			world = world * particle.orientation;
			instances.push_back(world.Transposed());
		}
		{
			// Draw each bundle
			int instance_count;
			auto* mesh = &fountain_particle_mesh;
			auto* bundle = &instances;
			instance_count = (int)bundle->size();
			if (instance_count > 0) {
				std::vector<float4x4> data(bundle->begin(), bundle->end());
				ID3D11ShaderResourceView* instance_srv = make_structured_buffer(dev, data.data(), instance_count, sizeof(float4x4));
				draw_solid_mesh(dev, ctx, &g_solid_shader, mesh, instance_srv, instance_count);
				instance_srv->Release();
			}
		}

		// Common tear-down
		matrix_cb->Release();
		light_srv->Release();
	}
};

static scene_factory_t<fountain_scene> g_fac_fountain;