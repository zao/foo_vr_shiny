#include <Windows.h>
#include <d3d11.h>
#include <dxgi.h>
#include "openvr.h"
#include "MathGeoLib.h"
#include "par_shapes.h"
#include "stb_image.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "openvr_api.lib")

#include <algorithm>
#include <deque>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "scene.h"
#include "d3d_helpers.h"
#include "vertex_mesh.h"
#include "shader_set.h"
#include "host_communicator.h"

scene_factory_base* scene_factory_base::first;

enum render_models_load_state {
	k_render_models_unloaded,
	k_render_models_loading,
	k_render_models_loaded,
};

render_models_load_state render_models_load = k_render_models_unloaded;

vr::IVRSystem* g_vr;
vr::IVRCompositor* g_comp;
vr::IVRRenderModels* g_render_models;

uint32_t g_width;
uint32_t g_height;

double g_now;

ID3D11Device* g_dev;
ID3D11DeviceContext* g_ctx;
D3D_FEATURE_LEVEL g_feature_level;
D3D11_VIEWPORT g_viewport;
ID3D11Texture2D* g_tex_eye[2];
ID3D11RenderTargetView* g_rtv_eye[2];
ID3D11DepthStencilView* g_dsv;
ID3D11DepthStencilState* g_depth_writes;
ID3D11DepthStencilState* g_skybox_depth;
ID3D11BlendState* g_solid_blend;
ID3D11RasterizerState* g_solid_raster;
ID3D11SamplerState* linear_sampler;

shader_set g_solid_shader;
shader_set g_skybox_shader;

std::wstring fs_root;

bool init_vr_d3d() {
	vr::EVRInitError ei_err;
	HRESULT hr;
	g_vr = vr::VR_Init(&ei_err, vr::VRApplication_Scene);
	if (!g_vr || ei_err != vr::VRInitError_None) {
		return false;
	}
	g_comp = vr::VRCompositor();
	if (!g_comp) {
		return false;
	}
	g_render_models = vr::VRRenderModels();
	if (!g_render_models) {
		return false;
	}
	uint32_t rec_width = 1, rec_height = 1;
	g_vr->GetRecommendedRenderTargetSize(&rec_width, &rec_height);
	g_width = rec_width;
	g_height = rec_height;
	hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, D3D11_CREATE_DEVICE_DEBUG, nullptr, 0, D3D11_SDK_VERSION, &g_dev, &g_feature_level, &g_ctx);
	if (!SUCCEEDED(hr)) {
		return false;
	}

	{
		ID3D11Debug* debug;
		if (SUCCEEDED(g_dev->QueryInterface(&debug))) {
			ID3D11InfoQueue* iq;
			if (SUCCEEDED(debug->QueryInterface(&iq))) {
				iq->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, TRUE);
				iq->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, TRUE);
				D3D11_MESSAGE_ID hide[] = {
					(D3D11_MESSAGE_ID)420, // STATE_CREATION WARNING #420: CREATEINPUTLAYOUT_EMPTY_LAYOUT
				};
				D3D11_INFO_QUEUE_FILTER filter = {};
				filter.DenyList.NumIDs = _countof(hide);
				filter.DenyList.pIDList = hide;
				iq->AddStorageFilterEntries(&filter);
				iq->Release();
			}
			debug->Release();
		}
	}


	g_viewport.Height = (float)g_height;
	g_viewport.MaxDepth = 1.0f;
	g_viewport.MinDepth = 0.0f;
	g_viewport.TopLeftX = 0.0f;
	g_viewport.TopLeftY = 0.0f;
	g_viewport.Width = (float)g_width;
	g_ctx->RSSetViewports(1, &g_viewport);

	{
		D3D11_TEXTURE2D_DESC td = {};
		td.ArraySize = 1;
		td.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		td.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		td.Height = g_height;
		td.MipLevels = 1;
		td.SampleDesc.Count = 1;
		td.SampleDesc.Quality = 0;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.Width = g_width;
		for (int i = 0; i < 2; ++i) {
			g_dev->CreateTexture2D(&td, nullptr, &g_tex_eye[i]);
			g_dev->CreateRenderTargetView(g_tex_eye[i], nullptr, &g_rtv_eye[i]);
		}
	}

	{
		ID3D11Texture2D* depth_tex;
		D3D11_TEXTURE2D_DESC td = {};
		td.ArraySize = 1;
		td.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		td.Format = DXGI_FORMAT_D32_FLOAT;
		td.Height = g_height;
		td.MipLevels = 1;
		td.SampleDesc.Count = 1;
		td.SampleDesc.Quality = 0;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.Width = g_width;
		hr = g_dev->CreateTexture2D(&td, nullptr, &depth_tex);
		hr = g_dev->CreateDepthStencilView(depth_tex, nullptr, &g_dsv);
		depth_tex->Release();
	}

	{
		D3D11_SAMPLER_DESC sd = {};
		sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sd.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		sd.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		sd.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		sd.MinLOD = -FLT_MAX;
		sd.MaxLOD = +FLT_MAX;
		sd.MipLODBias = 0.0f;
		sd.MaxAnisotropy = 1;
		sd.ComparisonFunc = D3D11_COMPARISON_NEVER;
		memcpy(sd.BorderColor, float4(1.0f, 1.0f, 1.0f, 1.0f).ptr(), sizeof(float4));
		hr = g_dev->CreateSamplerState(&sd, &linear_sampler);
	}

	D3D11_DEPTH_STENCIL_DESC dsd = {};
	dsd.DepthEnable = TRUE;
	dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsd.DepthFunc = D3D11_COMPARISON_LESS;
	dsd.StencilEnable = FALSE;
	dsd.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	dsd.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	dsd.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsd.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	dsd.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsd.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsd.BackFace = dsd.FrontFace;
	hr = g_dev->CreateDepthStencilState(&dsd, &g_depth_writes);

	dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsd.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	hr = g_dev->CreateDepthStencilState(&dsd, &g_skybox_depth);

	D3D11_BLEND_DESC bd = {};
	bd.AlphaToCoverageEnable = FALSE;
	bd.IndependentBlendEnable = FALSE;
	auto& rt = bd.RenderTarget[0];
	rt.BlendEnable = FALSE;
	rt.SrcBlend = D3D11_BLEND_ONE;
	rt.DestBlend = D3D11_BLEND_ZERO;
	rt.BlendOp = D3D11_BLEND_OP_ADD;
	rt.SrcBlendAlpha = D3D11_BLEND_ONE;
	rt.DestBlendAlpha = D3D11_BLEND_ZERO;
	rt.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rt.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	for (int i = 1; i < 8; ++i) {
		bd.RenderTarget[i] = rt;
	}
	hr = g_dev->CreateBlendState(&bd, &g_solid_blend);

	D3D11_RASTERIZER_DESC rd = {};
	rd.FillMode = D3D11_FILL_SOLID;
	rd.CullMode = D3D11_CULL_BACK;
	rd.DepthClipEnable = TRUE;
	hr = g_dev->CreateRasterizerState(&rd, &g_solid_raster);

	return true;
}

bool slurp_asset(wchar_t const* filename, std::vector<char>& out) {
	std::wstring path = fs_root + L"\\" + filename;
	HANDLE fh = CreateFileW(path.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
	if (fh == INVALID_HANDLE_VALUE) {
		return false;
	}
	uint64_t cb;
	if (!GetFileSizeEx(fh, (LARGE_INTEGER*)&cb)) {
		CloseHandle(fh);
		return false;
	}
	out.resize((size_t)cb);
	DWORD num_read = 0;
	if (!ReadFile(fh, out.data(), (DWORD)out.size(), &num_read, nullptr) || num_read != cb) {
		CloseHandle(fh);
		return false;
	}
	CloseHandle(fh);
	return true;
}

bool load_shaders() {
	HRESULT hr;
	std::vector<char> bytes;
	D3D11_INPUT_ELEMENT_DESC ied;

	if (!slurp_asset(L"solid.v.cso", bytes)) {
		return false;
	}
	hr = g_dev->CreateVertexShader(bytes.data(), bytes.size(), nullptr, &g_solid_shader.vs);
	if (!SUCCEEDED(hr)) {
		return false;
	}

	hr = g_dev->CreateInputLayout(&ied, 0, bytes.data(), (SIZE_T)bytes.size(), &g_solid_shader.il);
	if (!SUCCEEDED(hr)) {
		return false;
	}

	if (!slurp_asset(L"solid.p.cso", bytes)) {
		return false;
	}
	hr = g_dev->CreatePixelShader(bytes.data(), bytes.size(), nullptr, &g_solid_shader.ps);
	if (!SUCCEEDED(hr)) {
		return false;
	}

	if (!slurp_asset(L"skybox.v.cso", bytes)) {
		return false;
	}
	hr = g_dev->CreateVertexShader(bytes.data(), bytes.size(), nullptr, &g_skybox_shader.vs);
	if (!SUCCEEDED(hr)) {
		return false;
	}

	hr = g_dev->CreateInputLayout(&ied, 0, bytes.data(), (SIZE_T)bytes.size(), &g_skybox_shader.il);
	if (!SUCCEEDED(hr)) {
		return false;
	}

	if (!slurp_asset(L"skybox.p.cso", bytes)) {
		return false;
	}
	hr = g_dev->CreatePixelShader(bytes.data(), bytes.size(), nullptr, &g_skybox_shader.ps);
	if (!SUCCEEDED(hr)) {
		return false;
	}

	return true;
}


vertex_mesh plane;
vertex_mesh sim_token;
vertex_mesh skybox_plane;
vertex_mesh hex_mesh;
vertex_mesh fountain_particle_mesh;

bool load_geometry() {
	plane = make_plane(g_dev, 2.0f, 2.0f);
	skybox_plane = make_plane(g_dev, 1.0f, 1.0f);
	{
		float dx = 0.1f;
		float dz = 0.1f;
		float dy = 0.3f;
		float4 positions[] = {
			float4(0.0f, +dy, 0.0f, 1.0f), // top
			float4(-dx, 0.0f, 0.0f, 1.0f), // left
			float4(0.0f, 0.0f, -dz, 1.0f), // back
			float4(+dx, 0.0f, 0.0f, 1.0f), // right
			float4(0.0f, 0.0f, +dz, 1.0f), // forward
			float4(0.0f, -dy, 0.0f, 1.0f), // bottom
		};
		float2 texcoords[] = {
			float2(0.5f / 4.0f, 0.0f),
			float2(1.5f / 4.0f, 0.0f),
			float2(2.5f / 4.0f, 0.0f),
			float2(3.5f / 4.0f, 0.0f),
			float2(0.0f / 4.0f, 0.5f),
			float2(1.0f / 4.0f, 0.5f),
			float2(2.0f / 4.0f, 0.5f),
			float2(3.0f / 4.0f, 0.5f),
			float2(4.0f / 4.0f, 0.5f),
			float2(0.5f / 4.0f, 1.0f),
			float2(1.5f / 4.0f, 1.0f),
			float2(2.5f / 4.0f, 1.0f),
			float2(3.5f / 4.0f, 1.0f),
		};
		vertex vertices[4 * 3 * 2] = {};
		for (int i = 0; i < 4; ++i) {
			int i_a = i;
			int i_b = (i+1);
			float4 pos_up = positions[0];
			float4 pos_a = positions[1 + i_a%4];
			float4 pos_b = positions[1 + i_b%4];
			float4 pos_down = positions[5];
			float2 tc_up = texcoords[i];
			float2 tc_a = texcoords[4 + i_a];
			float2 tc_b = texcoords[4 + i_b];
			float2 tc_down = texcoords[9 + i];
			float3 normal_up = Cross3((pos_b - pos_up), (pos_a - pos_up)).Normalized3().xyz();
			float3 normal_down = Cross3((pos_a - pos_down), (pos_b - pos_down)).Normalized3().xyz();
			vertex slice_verts[] = {
				{ positions[0], normal_up, 0.0f, tc_up, float2::zero },
				{ pos_a, normal_up, 0.0f, tc_a, float2::zero },
				{ pos_b, normal_up, 0.0f, tc_b, float2::zero },
				{ pos_b, normal_down, 0.0f, tc_b, float2::zero },
				{ pos_a, normal_down, 0.0f, tc_a, float2::zero },
				{ pos_down, normal_down, 0.0f, tc_down, float2::zero },
			};
			memcpy(vertices + i*6, slice_verts, sizeof(slice_verts));
		}

		uint32_t indices[4*3*2] = {};
		for (int i = 0; i < _countof(indices); ++i) {
			indices[i] = i;
		}
		sim_token = make_vertex_mesh(g_dev, _countof(indices), indices, _countof(vertices), vertices);
	}
	{
#if 0
		par_shapes_mesh* cyl = par_shapes_create_cylinder(6, 3);
		par_shapes_rotate(cyl, pi / 2.0f, float3::unitX.ptr());
		par_shapes_scale(cyl, 0.2f, 0.2f, 0.2f);
		par_shapes_compute_normals(cyl);
#else
		par_shapes_mesh* cyl = par_shapes_create_subdivided_sphere(4);
		par_shapes_scale(cyl, 0.7f, 0.1f, 0.7f);
		par_shapes_compute_normals(cyl);
#endif
		hex_mesh = convert_par_shapes_mesh(g_dev, cyl);
		par_shapes_free_mesh(cyl);
	}
	{
		par_shapes_mesh* m = par_shapes_create_subdivided_sphere(3);
		par_shapes_scale(m, 0.05f, 0.05f, 0.05f);
		par_shapes_compute_normals(m);
		fountain_particle_mesh = convert_par_shapes_mesh(g_dev, m);
		par_shapes_free_mesh(m);
	}
	return true;
}

ID3D11ShaderResourceView* skybox_srv;
ID3D11ShaderResourceView* dark_skybox_srv;

int compute_mip_extent(int extent, int mip) {
	extent >>= mip;
	return extent ? extent : 1;
}

int compute_mip_level_count(int width, int height) {
	int mip_level = 0;
	while (compute_mip_extent(width, mip_level) != 1 && compute_mip_extent(height, mip_level) != 1) {
		++mip_level;
	}
	return mip_level + 1;
}

ID3D11ShaderResourceView* load_cubemap(wchar_t const* const* filenames) {
	HRESULT hr;
	ID3D11ShaderResourceView* srv = nullptr;
	uint8_t* image_pixels[6] = {};
	int width = 0, height = 0;
	bool everything_good = true;
	for (int i = 0; i < 6; ++i) {
		std::vector<char> bytes;
		if (slurp_asset(filenames[i], bytes)) {
			int image_width = 1, image_height = 1, comp = 4;
			uint8_t* pixels = stbi_load_from_memory((uint8_t*)bytes.data(), (int)bytes.size(), &image_width, &image_height, &comp, comp);
			if (pixels == nullptr) {
				everything_good = false;
			}
			if (width == 0 && height == 0) {
				width = image_width;
				height = image_height;
			}
			if (width != image_width || height != image_height) {
				everything_good = false;
			}
			image_pixels[i] = pixels;
		}
		else {
			everything_good = false;
		}
	}
	if (everything_good) {
		int mip_levels = 1; // compute_mip_level_count(width, height);
		D3D11_TEXTURE2D_DESC td = {};
		td.ArraySize = 6;
		td.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		td.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		td.Height = height;
		td.MipLevels = mip_levels;
		td.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS | D3D11_RESOURCE_MISC_TEXTURECUBE;
		td.SampleDesc.Count = 1;
		td.SampleDesc.Quality = 0;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.Width = width;
		std::vector<D3D11_SUBRESOURCE_DATA> srds(6 * mip_levels);
		for (int i = 0; i < 6; ++i) {
			D3D11_SUBRESOURCE_DATA srd = {};
			srd.pSysMem = image_pixels[i];
			srd.SysMemPitch = width * 4;
			auto I = srds.begin() + i * mip_levels;
			std::fill_n(I, mip_levels, srd);
		}

		ID3D11Texture2D* tex = nullptr;
		hr = g_dev->CreateTexture2D(&td, srds.data(), &tex);

		D3D11_SHADER_RESOURCE_VIEW_DESC sd = {};
		sd.Format = DXGI_FORMAT_UNKNOWN;
		sd.TextureCube.MostDetailedMip = 0;
		sd.TextureCube.MipLevels = -1;
		sd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		hr = g_dev->CreateShaderResourceView(tex, &sd, &srv);

		g_ctx->GenerateMips(srv);
		tex->Release();
	}
	for (int i = 0; i < 6; ++i) {
		stbi_image_free(image_pixels[i]);
	}
	return srv;
}

bool load_textures() {
	{
		wchar_t const* filenames[] = {
			L"TropicalSunnyDayLeft2048.png",
			L"TropicalSunnyDayRight2048.png",
			L"TropicalSunnyDayUp2048.png",
			L"TropicalSunnyDayDown2048.png",
			L"TropicalSunnyDayFront2048.png",
			L"TropicalSunnyDayBack2048.png",
		};
		skybox_srv = load_cubemap(filenames);
	}
	{
		wchar_t const* filenames[] = {
			L"DarkStormyLeft2048.png",
			L"DarkStormyRight2048.png",
			L"DarkStormyUp2048.png",
			L"DarkStormyDown2048.png",
			L"DarkStormyFront2048.png",
			L"DarkStormyBack2048.png",
		};
		dark_skybox_srv = load_cubemap(filenames);
	}
	return skybox_srv != nullptr && dark_skybox_srv != nullptr;
}

float2 mgl_vec2(float const* p) {
	return float2(p[0], p[1]);
}

float3 mgl_vec3(vr::HmdVector3_t v) {
	return float3(v.v);
}

float4 mgl_vector(vr::HmdVector3_t v) {
	return float4(v.v[0], v.v[1], v.v[2], 0.0f);
}

float4 mgl_point(vr::HmdVector3_t v) {
	return float4(v.v[0], v.v[1], v.v[2], 1.0f);
}

float4 mgl_vec4(vr::HmdVector4_t v) {
	return float4(v.v);
}

float3x4 mgl_mtx(vr::HmdMatrix34_t mtx) {
	return *(float3x4*)&mtx;
}

float4x4 mgl_mtx(vr::HmdMatrix44_t mtx) {
	return *(float4x4*)&mtx;
}

struct render_model_info {
	bool present;
	render_models_load_state state;
	std::string name;
};

render_model_info model_infos[vr::k_unMaxTrackedDeviceCount];
std::map<std::string, vr::RenderModel_t*> models;
std::map<vr::RenderModel_t*, vertex_mesh> device_meshes;

void refresh_render_models() {
	char name_buf[vr::k_unMaxPropertyStringSize] = {};
	for (int i = 0; i < _countof(model_infos); ++i) {
		if (model_infos[i].state == k_render_models_unloaded) {
			vr::ETrackedPropertyError prop_error;
			g_vr->GetStringTrackedDeviceProperty(i, vr::Prop_RenderModelName_String, name_buf, _countof(name_buf), &prop_error);
			if (prop_error == vr::TrackedProp_Success) {
				std::string name = name_buf;
				if (models.find(name) == models.end()) {
					models[name];
				}
			}
		}
	}
	for (auto I = models.begin(); I != models.end(); ++I) {
		if (I->second != nullptr) {
			continue;
		}
		int n = (int)g_render_models->GetRenderModelCount();
		for (int i = 0; i < n; ++i) {
			g_render_models->GetRenderModelName(i, name_buf, _countof(name_buf));
			if (I->first == name_buf) {
				vr::EVRRenderModelError rm_err = g_render_models->LoadRenderModel_Async(name_buf, &I->second);
				if (rm_err == vr::VRRenderModelError_None) {
					vr::RenderModel_t* rm = I->second;
					std::vector<uint32_t> indices(rm->unTriangleCount * 3);
					std::vector<vertex> vertices(rm->unVertexCount);
					for (int i = 0; i < (int)rm->unTriangleCount; ++i) {
						indices[3*i+0] = rm->rIndexData[3*i+2];
						indices[3*i+1] = rm->rIndexData[3*i+1];
						indices[3*i+2] = rm->rIndexData[3*i+0];
					}
					for (int i = 0; i < (int)vertices.size(); ++i) {
						vr::RenderModel_Vertex_t const* src = rm->rVertexData + i;
						vertex dst = {};
						dst.position = mgl_point(src->vPosition);
						dst.normal = mgl_vec3(src->vPosition);
						dst.texcoord = mgl_vec2(src->rfTextureCoord);
						vertices[i] = dst;
					}
					device_meshes[rm] = make_vertex_mesh(g_dev, (int)indices.size(), indices.data(), (int)vertices.size(), vertices.data());
				}
			}
		}
	}
}

spectrum g_spectrum;
waveform g_waveform;

struct instance {
	float4x4 world;
};

std::vector<std::shared_ptr<scene>> g_background_scenes;
std::vector<std::shared_ptr<scene>> g_foreground_scenes;

int g_current_background_scene;
int g_current_foreground_scene;

scene* get_current_scene(scene::flavor flavor) {
	switch (flavor) {
	case scene::k_background_scene: return !g_background_scenes.empty() ? g_background_scenes[g_current_background_scene].get() : nullptr;
	case scene::k_foreground_scene: return !g_foreground_scenes.empty() ? g_foreground_scenes[g_current_foreground_scene].get() : nullptr;
	}
	return nullptr;
}

void draw_stuff(scene_info const& si, int eye) {
	float clear_color[4] = { 0.1f, 0.2f, 0.3f, 1.0f };
	g_ctx->ClearRenderTargetView(si.rtvs[eye], clear_color);
	g_ctx->ClearDepthStencilView(g_dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
	g_ctx->OMSetRenderTargets(1, &si.rtvs[eye], g_dsv);

	if (!g_background_scenes.empty()) {
		auto scn = g_background_scenes.at(g_current_background_scene);
		scn->draw(si, eye);
	}
	if (!g_foreground_scenes.empty()) {
		auto scn = g_foreground_scenes.at(g_current_foreground_scene);
		scn->draw(si, eye);
	}

	vr::Texture_t tex = {};
	tex.eColorSpace = vr::ColorSpace_Gamma;
	tex.eType = vr::API_DirectX;
	tex.handle = si.texs[eye];
	vr::EVRCompositorError comp_err;
	comp_err = g_comp->Submit((vr::EVREye)eye, &tex);
	switch (comp_err) {
	case vr::VRCompositorError_None: break;
	default: DebugBreak(); break;
	}
}

int left_controller_id;
int right_controller_id;

void on_messages(std::string* messages, int message_count) {
	for (int i = 0; i < message_count; ++i) {
		auto& msg = messages[i];
		std::istringstream iss(msg);
		std::string op;
		if (iss >> op) {
			if (op == "spectrum") {
				int width;
				if (iss >> width) {
					width /= 10;
					g_spectrum.values.resize(width);
					for (int sample = 0; sample < width; ++sample) {
						float2 value = float2::zero;
						iss >> value.x;
						g_spectrum.values[sample] = value;
					}
				}
			}
			else if (op == "waveform") {
				int width;
				if (iss >> width) {
					g_waveform.values.resize(width);
					for (int bucket = 0; bucket < width; ++bucket) {
						float3 value = float3::zero;
						iss >> value.x >> value.y >> value.z;
						g_waveform.values[bucket] = value;
					}
				}
			}
		}
	}
}

void advance_scene(scene::flavor which, int amount) {
	std::vector<std::shared_ptr<scene>>* scene_collection = nullptr;
	int* scene_index = nullptr;
	switch (which) {
	case scene::k_background_scene: {
		scene_collection = &g_background_scenes;
		scene_index = &g_current_background_scene;
									} break;
	case scene::k_foreground_scene: {
		scene_collection = &g_foreground_scenes;
		scene_index = &g_current_foreground_scene;
									} break;
	default: std::terminate();
	}
	if (!scene_collection->empty()) {
		int n = (int)scene_collection->size();
		get_current_scene(which)->deactivate();
		while (amount < 0) {
			amount += n;
		}
		*scene_index = (*scene_index + amount) % n;
		get_current_scene(which)->activate(g_now);
	}
}

int WINAPI wWinMain(HINSTANCE instance, HINSTANCE, LPWSTR cmd_line, int) {
	int argc;
	LPWSTR* argv = CommandLineToArgvW(cmd_line, &argc);
	bool hosted = false;
	for (int i = 0; i < argc; ++i) {
		if (wcscmp(argv[i], L"-hosted") == 0) {
			hosted = true;
		}
	}
	{
		wchar_t file_path[1<<16] = {};
		GetModuleFileNameW(nullptr, file_path, _countof(file_path));
		*wcsrchr(file_path, L'\\') = L'\0';
		fs_root = file_path;
	}
	setup_communicator(hosted);

	if (!init_vr_d3d()) {
		return 1;
	}
	refresh_render_models();
	if (!load_shaders()) {
		return 1;
	}
	if (!load_geometry()) {
		return 1;
	}
	if (!load_textures()) {
		return 1;
	}

	{
		std::vector<scene_factory_base*> facs;
		for (auto* p = scene_factory_base::first; p; p = p->next) {
			facs.push_back(p);
		}
		std::sort(facs.begin(), facs.end(), [](scene_factory_base* a, scene_factory_base* b) -> bool {
			return a->get_name() < b->get_name();
		});
		for (auto I = facs.begin(); I != facs.end(); ++I) {
			auto* fac = *I;
			auto scn = fac->create(g_dev, g_ctx);
			switch (fac->get_flavor()) {
			case scene::k_background_scene: g_background_scenes.push_back(scn); break;
			case scene::k_foreground_scene: g_foreground_scenes.push_back(scn); break;
			}
		}
	}

	{
		if (auto* bg_scene = get_current_scene(scene::k_background_scene)) {
			bg_scene->activate(g_now);
		}
		if (auto* fg_scene = get_current_scene(scene::k_foreground_scene)) {
			fg_scene->activate(g_now);
		}
	}

	float3x4 debug_world_shift = float3x4::Translate(-1.0f, 0.0f, 3.0f) * float3x4::RotateY(pi);
	
	scene_info scene = {};
	float4x4 projs[] = {
		mgl_mtx(g_vr->GetProjectionMatrix(vr::Eye_Left, 0.1f, 100.0f, vr::API_DirectX)),
		mgl_mtx(g_vr->GetProjectionMatrix(vr::Eye_Right, 0.1f, 100.0f, vr::API_DirectX)),
	};
	memcpy(scene.projs, projs, sizeof(projs));
	memcpy(scene.rtvs, g_rtv_eye, sizeof(g_rtv_eye));
	memcpy(scene.texs, g_tex_eye, sizeof(g_tex_eye));

	LCG fake_spectrum_lcg(9001u);

	uint64_t time_freq;
	QueryPerformanceFrequency((LARGE_INTEGER*)&time_freq);
	uint64_t time_base;
	QueryPerformanceCounter((LARGE_INTEGER*)&time_base);
	uint64_t time_snap;
	while (1) {
		refresh_render_models();
		QueryPerformanceCounter((LARGE_INTEGER*)&time_snap);
		g_now = (time_snap - time_base) / (double)time_freq;
		if (!pump_communicator(&on_messages)) {
			break;
		}
		if (!hosted) {
			g_spectrum.values.resize(128);
			for (int i = 0; i < (int)g_spectrum.values.size(); ++i) {
				g_spectrum.values[i] = float2(0.0f + 0.5f * sinf((float)g_now + i * 0.1f), 0.0f);
			}
			std::vector<char> raw_waveform;
			if (slurp_asset(L"run.dat", raw_waveform) && raw_waveform.size() > sizeof(uint16_t)) {
				char* p = raw_waveform.data();
				uint16_t bucket_count = 0;
				memcpy(&bucket_count, p, sizeof(uint16_t));
				p += sizeof(uint16_t);
				if (raw_waveform.size() == sizeof(uint16_t) + bucket_count * (sizeof(int16_t) + sizeof(int16_t) + sizeof(uint16_t))) {
					g_waveform.values.resize(bucket_count);
					for (int bucket = 0; bucket < bucket_count; ++bucket) {
						int16_t v;
						memcpy(&v, p, sizeof(int16_t));
						g_waveform.values[bucket][0] = (float)v / (v < 0 ? -SHRT_MIN : SHRT_MAX);
						p += sizeof(int16_t);
					}
					for (int bucket = 0; bucket < bucket_count; ++bucket) {
						int16_t v;
						memcpy(&v, p, sizeof(int16_t));
						g_waveform.values[bucket][1] = (float)v / (v < 0 ? -SHRT_MIN : SHRT_MAX);
						p += sizeof(int16_t);
					}
					for (int bucket = 0; bucket < bucket_count; ++bucket) {
						uint16_t v;
						memcpy(&v, p, sizeof(uint16_t));
						g_waveform.values[bucket][2] = (float)v / USHRT_MAX;
						p += sizeof(uint16_t);
					}
				}
			}
		}
		vr::VREvent_t event;
		while (g_vr->PollNextEvent(&event, sizeof(event))) {
			switch (event.eventType) {
			case vr::VREvent_ButtonPress: {
				if (event.data.controller.button == vr::k_EButton_SteamVR_Touchpad) {
					advance_scene(scene::k_background_scene, +1);
				}
				if (event.data.controller.button == vr::k_EButton_Grip) {
					advance_scene(scene::k_foreground_scene, +1);
				}
				std::ostringstream oss;
				oss << "Pressed button " << event.data.controller.button;
				oss << '\n';
				OutputDebugStringA(oss.str().c_str());
										  } break;
			default: {
				std::ostringstream oss;
				oss << g_vr->GetEventTypeNameFromEnum((vr::EVREventType)event.eventType);
				oss << '\n';
				OutputDebugStringA(oss.str().c_str());
					 } break;
			}
		}
		vr::TrackedDevicePose_t poses[vr::k_unMaxTrackedDeviceCount] = {};
		g_comp->WaitGetPoses(poses, _countof(poses), nullptr, 0);
		float3x4 view;
		for (int i = 0; i < _countof(poses); ++i) {
			auto pose = poses + i;
			if (g_vr->GetTrackedDeviceClass(i) == vr::TrackedDeviceClass_HMD) {
				view = mgl_mtx(pose->mDeviceToAbsoluteTracking).Inverted();
			}
			if (g_vr->GetTrackedDeviceClass(i) == vr::TrackedDeviceClass_Controller) {
				auto role = g_vr->GetControllerRoleForTrackedDeviceIndex(i);
				if (role == vr::TrackedControllerRole_LeftHand) {
					scene.controller_world[0] = mgl_mtx(pose->mDeviceToAbsoluteTracking);
					scene.controller_model[0] = i;
					left_controller_id = i;
				}
				if (role == vr::TrackedControllerRole_RightHand) {
					scene.controller_world[1] = mgl_mtx(pose->mDeviceToAbsoluteTracking);
					scene.controller_model[1] = i;
					right_controller_id = i;
				}
			}
		}
		float3x4 eyes[] = {
			mgl_mtx(g_vr->GetEyeToHeadTransform(vr::Eye_Left)),
			mgl_mtx(g_vr->GetEyeToHeadTransform(vr::Eye_Right)),
		};

		memcpy(scene.eyes, eyes, sizeof(eyes));
		scene.view = view * debug_world_shift;
		// scene.view = view;

		if (!g_background_scenes.empty()) {
			g_background_scenes.at(g_current_background_scene)->update(g_now);
		}

		if (!g_foreground_scenes.empty()) {
			g_foreground_scenes.at(g_current_foreground_scene)->update(g_now);
		}

		for (int i = 0; i < 2; ++i) {
			draw_stuff(scene, i);
		}
	}
	vr::VR_Shutdown();
	teardown_communicator();
	return 0;
}