#pragma once

#include <d3d11.h>

struct shader_set {
	ID3D11VertexShader* vs;
	ID3D11PixelShader* ps;
	ID3D11InputLayout* il;
};