#pragma once

#include <d3d11.h>

inline ID3D11Buffer* make_constant_buffer(ID3D11Device* dev, void* data, int size) {
	HRESULT hr;

	D3D11_BUFFER_DESC bd = {};
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.ByteWidth = size;
	bd.Usage = D3D11_USAGE_IMMUTABLE;
		
	D3D11_SUBRESOURCE_DATA srd = {};
	srd.pSysMem = data;
		
	ID3D11Buffer* cb;
	hr = dev->CreateBuffer(&bd, &srd, &cb);
	return cb;
}

inline ID3D11ShaderResourceView* make_structured_buffer(ID3D11Device* dev, void* data, int count, int stride) {
	HRESULT hr;

	D3D11_BUFFER_DESC bd = {};
	bd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	bd.ByteWidth = count * stride;
	bd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	bd.StructureByteStride = stride;
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	
	D3D11_SUBRESOURCE_DATA srd = {};
	srd.pSysMem = data;

	ID3D11Buffer* buf;
	hr = dev->CreateBuffer(&bd, &srd, &buf);
	
	ID3D11ShaderResourceView* srv;
	hr = dev->CreateShaderResourceView(buf, nullptr, &srv);
	
	buf->Release();
	return srv;
}
