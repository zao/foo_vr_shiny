#pragma once

#include <memory>
#include <string>

#include <Guiddef.h>
#include <d3d11.h>
#include "MathGeoLib.h"

struct scene_info {
	float3x4 eyes[2];
	float4x4 projs[2];
	float3x4 view;
	ID3D11RenderTargetView* rtvs[2];
	ID3D11Texture2D* texs[2];
	float3x4 controller_world[2];
	int controller_model[2];
};

struct spectrum {
	std::vector<float2> values;
};

struct waveform {
	std::vector<float3> values;
};

extern spectrum g_spectrum;
extern waveform g_waveform;

struct scene {
	enum flavor {
		k_background_scene,
		k_foreground_scene,
	};
	virtual ~scene() {}

	virtual void activate(double now) = 0;
	virtual void deactivate() = 0;

	virtual void update(double now) = 0;
	virtual void draw(scene_info const& si, int eye) = 0;
};

struct scene_factory_base {
	scene_factory_base() {
		next = first;
		first = this;
	}
	static scene_factory_base* first;
	scene_factory_base* next;

	virtual scene::flavor get_flavor() = 0;
	virtual GUID get_guid() = 0;
	virtual std::string get_name() = 0;
	virtual std::shared_ptr<scene> create(ID3D11Device* dev, ID3D11DeviceContext* ctx) = 0;
};

template <typename Scene>
struct scene_factory_t : scene_factory_base {
	virtual scene::flavor get_flavor() override {
		return Scene::g_get_flavor();
	}

	virtual GUID get_guid() override {
		return Scene::g_get_guid();
	}

	virtual std::string get_name() override {
		return Scene::g_get_name();
	}

	std::shared_ptr<scene> create(ID3D11Device* dev, ID3D11DeviceContext* ctx) override {
		return std::make_shared<Scene>(dev, ctx);
	}
};