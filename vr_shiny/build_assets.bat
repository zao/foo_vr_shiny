VERIFY OTHER 2>nul
SETLOCAL ENABLEEXTENSIONS
SET FXC=C:\dev\dxsdk\Utilities\Bin\x64\fxc.exe
SET DST=%1
IF DEFINED DST (set DST) ELSE (SET DST=..\portable\user-components\foo_vr_shiny\)

%FXC% /Tvs_5_0 /Emain /O3 /Fo%DST%solid.v.cso shaders\solid.v.hlsl
%FXC% /Tps_5_0 /Emain /O3 /Fo%DST%solid.p.cso shaders\solid.p.hlsl
%FXC% /Tvs_5_0 /Emain /O3 /Fo%DST%skybox.v.cso shaders\skybox.v.hlsl
%FXC% /Tps_5_0 /Emain /O3 /Fo%DST%skybox.p.cso shaders\skybox.p.hlsl