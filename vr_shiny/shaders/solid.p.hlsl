struct ps_in_data {
	float4 position : SV_Position;
	float3 view_position : ViewPosition;
	float3 normal : ViewNormal;
	float2 texcoord : Texcoord;
};

struct light {
	float3 dir_pos; uint is_point;
	float3 color; float _pad1;
};

StructuredBuffer<light> lights : register(t0);

float4 main(ps_in_data ps_in) : SV_Target {
	float3 n = normalize(ps_in.normal);
	float3 lc = float3(0.0, 0.0, 0.0);
	uint light_count, light_stride;
	lights.GetDimensions(light_count, light_stride);
	for (uint i = 0; i < light_count; ++i) {
		light li = lights[i];
		float3 l = li.is_point ? normalize(li.dir_pos - ps_in.view_position.xyz) : -li.dir_pos;
		float3 hue = li.color;
		lc += hue * saturate(dot(n, l));
	}
	float4 color = float4(lc, 1.0);
	return color;
}