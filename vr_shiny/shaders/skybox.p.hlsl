struct ps_in_data {
	float4 position : SV_Position;
	float3 direction : Direction;
};

SamplerState linear_cube : register(s0);
TextureCube skybox : register(t1);

float4 main(ps_in_data ps_in) : SV_Target {
	float4 color = float4(ps_in.direction, 1.0);
	color = skybox.Sample(linear_cube, normalize(ps_in.direction));
	return color;
}