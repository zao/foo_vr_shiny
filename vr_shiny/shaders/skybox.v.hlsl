struct vertex {
	float4 position;
	float4 _pad0;
	float4 _pad1;
};

struct vs_out_data {
	float4 ndc_position : SV_Position;
	float3 direction : Direction;
};

cbuffer vs_matrices : register(b0)
{
	float4x4 proj;
	float4x4 view;
};

StructuredBuffer<vertex> vertices : register(t0);
StructuredBuffer<float4x4> instances : register(t1);

vs_out_data main(uint vid : SV_VertexID, uint iid : SV_InstanceID) {
	vs_out_data vs_out;
	vertex vtx = vertices[vid];
	float4x4 inst = instances[iid];
	float4 world_position = mul(inst, vtx.position);
	vs_out.direction = world_position.xyz * float3(+1.0, +1.0, +1.0);
	float4 ndc = mul(proj, mul(view, float4(world_position.xyz, 0.0)));
	vs_out.ndc_position = ndc.xyww;
	return vs_out;
}