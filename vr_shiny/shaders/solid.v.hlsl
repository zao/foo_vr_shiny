struct vertex {
	float4 position;
	float3 normal; float _pad0;
	float2 texcoord; float2 _pad1;
};

struct vs_out_data {
	float4 ndc_position : SV_Position;
	float3 view_position : ViewPosition;
	float3 view_normal : ViewNormal;
	float2 texcoord : Texcoord;
};

cbuffer vs_matrices : register(b0)
{
	float4x4 proj;
	float4x4 view;
};

StructuredBuffer<vertex> vertices : register(t0);
StructuredBuffer<float4x4> instances : register(t1);

vs_out_data main(uint vid : SV_VertexID, uint iid : SV_InstanceID) {
	vs_out_data vs_out;
	vertex vtx = vertices[vid];
	float4x4 inst = instances[iid];
	float4 view_position = mul(view, mul(inst, vtx.position));
	vs_out.view_position = view_position.xyz;
	vs_out.ndc_position = mul(proj, view_position);
	vs_out.view_normal = mul((float3x3)view, mul((float3x3)inst, vtx.normal));
	vs_out.texcoord = vtx.texcoord;
	return vs_out;
}