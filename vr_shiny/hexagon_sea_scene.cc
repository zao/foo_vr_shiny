#include "scene.h"
#include "d3d_helpers.h"
#include "vertex_mesh.h"
#include "shader_set.h"

#include <algorithm>
#include <deque>
#include <map>

extern shader_set g_solid_shader;

extern vertex_mesh skybox_plane;
extern vertex_mesh hex_mesh;
extern ID3D11ShaderResourceView* skybox_srv;

struct hexagon_sea_scene : scene {
	static flavor g_get_flavor() { return k_background_scene; }
	
	static GUID g_get_guid() {
		// {A8766A9C-0B89-4DD5-9813-E776C8C964F1}
		static GUID const s_guid = {
			0xa8766a9c, 0xb89, 0x4dd5, { 0x98, 0x13, 0xe7, 0x76, 0xc8, 0xc9, 0x64, 0xf1 }
		};
		return s_guid;
	}

	static std::string g_get_name() { return "Hexagon sea"; }

	hexagon_sea_scene(ID3D11Device* dev, ID3D11DeviceContext* ctx) {
		this->dev = dev;
		this->ctx = ctx;
		vertical_spacing = 1.0f;
		axis_extent = 20;
		propagation_interval = 0.1f;
	}

	struct hexagon {
		float2 pos;
		float oomph;
	};

	ID3D11Device* dev;
	ID3D11DeviceContext* ctx;

	float vertical_spacing;
	float horizontal_spacing;
	int axis_extent;

	struct axial_coordinate {
		int q, r;
		bool operator < (axial_coordinate rhs) const { if (q != rhs.q) return q < rhs.q; return r < rhs.r; }
		operator float2 () const { return float2((float)q, (float)r); }
	};

	std::map<axial_coordinate, hexagon> hexes;

	void activate(double now) override {
		oomphs.clear();
		last_oomph = now;
		LCG lcg(9001u);
		auto ex = axis_extent;
		for (int x = -ex; x <= ex; ++x) {
			for (int y = (std::max)(-ex, -x-ex); y <= (std::min)(ex, -x+ex); ++y) {
				int z = -x-y;
				axial_coordinate axial = { x, z };
				int dist = (abs(axial.q) + abs(axial.q + axial.r) + abs(axial.r)) / 2;
				if (dist >= 2) {
					float2 Q = vertical_spacing * float2(sqrtf(3.0f), sqrtf(3.0f) / 2.0f);
					float2 R = vertical_spacing * float2(0.0f, 1.5f);
					float2 pos = float2(Dot(Q, axial), Dot(R, axial));
					hexagon hex = { pos, 0.0f };
					hexes[axial] = hex;
				}
			}
		}
	}

	void deactivate() override {
		hexes.clear();
	}

	std::deque<float> oomphs;
	double last_oomph;
	double propagation_interval;

	void update(double now) override {
		auto max_x = [](float2 a, float2 b) -> bool { return a.x < b.x; };
		float top = std::max_element(g_spectrum.values.begin(), g_spectrum.values.begin() + (std::max<size_t>)(g_spectrum.values.size(), 16), max_x)->x;
		oomphs.push_front(top);
		if (oomphs.size() > 1500) {
			oomphs.resize(1500);
		}
		for (auto I = hexes.begin(); I != hexes.end(); ++I) {
			axial_coordinate ac = I->first;
#if 0
			int dist = (abs(ac.q) + abs(ac.q + ac.r) + abs(ac.r)) / 2;
			float oomph = oomphs.size() > dist ? oomphs[dist] : 0.0f;
#else
			float len = I->second.pos.Length();
			int dist = (int)len;
			float frac = len - dist;
			float oomph_a = oomphs.size() > dist ? oomphs[dist] : 0.0f;
			float oomph_b = oomphs.size() > (dist+1) ? oomphs[dist+1] : 0.0f;
			float oomph = Lerp(oomph_a, oomph_b, frac);
#endif
			I->second.oomph = oomph;
		}
	}

	void draw(scene_info const& si, int eye) override {
		struct vs_matrices {
			float4x4 proj;
			float4x4 view;
		};
		
		vs_matrices mats = {};
		mats.proj = si.projs[eye].Transposed();
		float4x4 view = float4x4(si.eyes[eye].Inverted() * si.view);
		mats.view = view.Transposed();
		ID3D11Buffer* matrix_cb = make_constant_buffer(dev, &mats, sizeof(mats));

		struct light {
			float3 dir_pos; uint32_t is_point;
			float3 color; float _pad1;
		};

		light sun = {
			-float3::unitY, 0,
			float3(0.1f, 0.1f, 0.1f), 0.0f,
		};
		light center = {
			float3(0.0f, 1.0f, 0.0f), 1,
			float3(1.0f, 1.0f, 1.0f), 0.0f,
		};
		sun.dir_pos = view.TransformDir(sun.dir_pos);
		center.dir_pos = view.TransformPos(center.dir_pos);
		light lights[] = { sun, center };
		ID3D11ShaderResourceView* light_srv = make_structured_buffer(dev, &lights, _countof(lights), sizeof(light));

		// Common setup
		ctx->VSSetConstantBuffers(0, 1, &matrix_cb);
		ctx->PSSetShaderResources(0, 1, &light_srv);

		LCG lcg(9001u);
		std::vector<float4x4> instances;
		instances.reserve(hexes.size());
		for (auto I = hexes.begin(); I != hexes.end(); ++I) {
			auto& hex = I->second;
			float4x4 world = float4x4::Translate(hex.pos[0], hex.oomph, hex.pos[1]);
			// world = world * float4x4::RotateAxisAngle(float3::RandomDir(lcg), 0.1f);
			instances.push_back(world.Transposed());
		}
		{
			// Draw each bundle
			int instance_count;
			auto* mesh = &hex_mesh;
			auto* bundle = &instances;
			instance_count = (int)bundle->size();
			if (instance_count > 0) {
				std::vector<float4x4> data(bundle->begin(), bundle->end());
				ID3D11ShaderResourceView* instance_srv = make_structured_buffer(dev, data.data(), instance_count, sizeof(float4x4));
				draw_solid_mesh(dev, ctx, &g_solid_shader, mesh, instance_srv, instance_count);
				instance_srv->Release();
			}
		}

		draw_skybox(dev, ctx, skybox_srv);

		// Common tear-down
		matrix_cb->Release();
		light_srv->Release();
	}
};

static scene_factory_t<hexagon_sea_scene> g_fac_hexagon_sea;