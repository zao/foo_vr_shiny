#pragma once

#include <string>

void setup_communicator(bool is_hosted);
void teardown_communicator();

typedef void (*message_process_callback)(std::string* messages, int message_count);
bool pump_communicator(message_process_callback cb);