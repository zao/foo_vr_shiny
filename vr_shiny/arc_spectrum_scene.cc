#include "scene.h"
#include "d3d_helpers.h"
#include "vertex_mesh.h"

extern vertex_mesh plane;
extern vertex_mesh fountain_particle_mesh;
extern shader_set g_solid_shader;
extern ID3D11ShaderResourceView* skybox_srv;

struct arc_spectrum_scene : scene {
	static flavor g_get_flavor() { return k_background_scene; }
	
	static GUID g_get_guid() {
		// {70F1DE25-0B58-4993-962B-7D111126A2D3}
		static GUID const s_guid = {
			0x70f1de25, 0xb58, 0x4993, { 0x96, 0x2b, 0x7d, 0x11, 0x11, 0x26, 0xa2, 0xd3 }
		};
		return s_guid;
	}
	
	static std::string g_get_name() { return "Arc spectrum"; }

	int g_dev;
	int g_ctx;

	arc_spectrum_scene(ID3D11Device* dev, ID3D11DeviceContext* ctx) {
		this->dev = dev;
		this->ctx = ctx;
	}

	ID3D11Device* dev;
	ID3D11DeviceContext* ctx;

	void activate(double now) override {}

	void deactivate() override {
		std::vector<float4x4> v;
		sim_token_instances.swap(v);
	}

	std::vector<float4x4> sim_token_instances;

	void update(double now) override {
		sim_token_instances.clear();
		float spread = pi/2;
		float separation = 0.02f;
		int spectrum_count = (int)g_spectrum.values.size();
		for (int sample = 0; sample < spectrum_count; ++sample) {
			float2 value = g_spectrum.values[sample];
			float theta = Lerp(spread, -spread, sample / (spectrum_count-1.0f));
			float3 pos = float3(0.0f, Lerp(0.5f, 2.0f, value.x), -1.0f);
			float4x4 world =
				float4x4::RotateY(theta) *
				float4x4::Translate(pos);
			float4x4 inst = world.Transposed();
			sim_token_instances.push_back(inst);
		}
	}

	void draw(scene_info const& si, int eye) override {
		struct vs_matrices {
			float4x4 proj;
			float4x4 view;
		};
		
		vs_matrices mats = {};
		mats.proj = si.projs[eye].Transposed();
		float4x4 view = float4x4(si.eyes[eye].Inverted() * si.view);
		mats.view = view.Transposed();
		ID3D11Buffer* matrix_cb = make_constant_buffer(dev, &mats, sizeof(mats));

		struct light {
			float3 dir_pos; uint32_t is_point;
			float3 color; float _pad1;
		};

		light sun = {
			view.TransformDir(-float3::unitY), 0,
			float3(0.1f, 0.1f, 0.1f), 0.0f,
		};

		std::vector<light> lights(1);
		lights[0] = sun;
		LCG lcg(9001u);
		for (int i = 0; i < 8; ++i) {
			float3 color = float3::RandomSphere(lcg, float3::zero, 0.5f).Abs();
			light lgt = {
				view.TransformPos(float3x3::RotateY(i / 8.0f).Transform(+float3::unitX)), 1,
				color, 0.0f,
			};
			lights.push_back(lgt);
		}

		ID3D11ShaderResourceView* light_srv = make_structured_buffer(dev, lights.data(), (int)lights.size(), sizeof(light));

		std::vector<float4x4> plane_instances(1);
		plane_instances[0] = float4x4::identity.Transposed();

		std::vector<float4x4> lefts, rights;
		lefts.push_back(si.controller_world[0]);
		rights.push_back(si.controller_world[1]);

		std::vector<float4x4>* bundles[4] = {
			&plane_instances, &sim_token_instances, &lefts, &rights,
		};

		vertex_mesh* meshes[4] = {
			&plane, &fountain_particle_mesh,
		};
	
		// Common setup
		ctx->VSSetConstantBuffers(0, 1, &matrix_cb);
		ctx->PSSetShaderResources(0, 1, &light_srv);

		for (int bundle_idx = 0; bundle_idx < _countof(bundles); ++bundle_idx) {
			// Draw each bundle
			int instance_count;
			auto* mesh = meshes[bundle_idx];
			auto* bundle = bundles[bundle_idx];
			if (mesh == nullptr || bundle == nullptr) {
				continue;
			}
			instance_count = (int)bundle->size();
			if (instance_count == 0) {
				continue;
			}

			std::vector<float4x4> data(bundle->begin(), bundle->end());
			ID3D11ShaderResourceView* instance_srv = make_structured_buffer(dev, data.data(), instance_count, sizeof(float4x4));
			draw_solid_mesh(dev, ctx, &g_solid_shader, mesh, instance_srv, instance_count);

			instance_srv->Release();
		}

		draw_skybox(dev, ctx, skybox_srv);

		// Common tear-down
		matrix_cb->Release();
		light_srv->Release();
	}
};

static scene_factory_t<arc_spectrum_scene> g_fac_arc_spectrum;