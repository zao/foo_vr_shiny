#include "scene.h"
#include "d3d_helpers.h"
#include "vertex_mesh.h"

#include <algorithm>

extern shader_set g_solid_shader;

struct waveform_bars_scene : scene {
	static flavor g_get_flavor() { return scene::k_foreground_scene; }
	
	static GUID g_get_guid() {
		// {65540DF9-873B-401E-9DA3-9A8AFF650774}
		static GUID const s_guid = {
			0x65540df9, 0x873b, 0x401e, { 0x9d, 0xa3, 0x9a, 0x8a, 0xff, 0x65, 0x7, 0x74 }
		};
		return s_guid;
	}
	
	static std::string g_get_name() { return "Waveform bar"; }

	waveform_bars_scene(ID3D11Device* dev, ID3D11DeviceContext* ctx)
		: dev(dev)
		, ctx(ctx)
		, waveform_base(0)
	{
		vertex_mesh _ = {};
		bar_box_mesh = _;
	}

	ID3D11Device* dev;
	ID3D11DeviceContext* ctx;

	int waveform_base;
	vertex_mesh bar_box_mesh;

	void activate(double now) override {}
	void deactivate() override {}

	std::vector<float4x4> boxes;

	void update(double now) override {
		float left = -50.0f;
		float right = 50.0f;
		int count = (int)g_waveform.values.size();

		if (waveform_base != count) {
			if (bar_box_mesh.ib) bar_box_mesh.ib->Release();
			if (bar_box_mesh.vb) bar_box_mesh.vb->Release();
			if (bar_box_mesh.vb_srv) bar_box_mesh.vb_srv->Release();
			float scale = 0.5f * (right - left) / count;
			par_shapes_mesh* m = par_shapes_create_cube();
			par_shapes_translate(m, -0.5f, -0.5f, -0.5f);
			par_shapes_scale(m, 0.9f * scale, 1.0f, 0.9f * scale);
			par_shapes_compute_normals(m);
			bar_box_mesh = convert_par_shapes_mesh(dev, m);
			par_shapes_free_mesh(m);
			waveform_base = count;
		}
		boxes.resize(count);
		for (int i = 0; i < count; ++i) {
			float x = Lerp(left, right, (i + 0.5f) / count);
			float3 sample = g_waveform.values[i];
			boxes[i] = float4x4::Translate(x, 0.5f, 0.0f) * float3x4::UniformScale((sample.y - sample.x) / 2.0f);
		}
	}
	
	void draw(scene_info const& si, int eye) override {
		struct vs_matrices {
			float4x4 proj;
			float4x4 view;
		};
		
		vs_matrices mats = {};
		mats.proj = si.projs[eye].Transposed();
		float4x4 view = float4x4(si.eyes[eye].Inverted() * si.view);
		mats.view = view.Transposed();
		ID3D11Buffer* matrix_cb = make_constant_buffer(dev, &mats, sizeof(mats));

		struct light {
			float3 dir_pos; uint32_t is_point;
			float3 color; float _pad1;
		};

		light sun = {
			view.TransformDir(-float3::unitY), 0,
			float3(0.1f, 0.1f, 0.1f), 0.0f,
		};

		std::vector<light> lights(1);
		lights[0] = sun;
		LCG lcg(9001u);
		for (int i = 0; i < 8; ++i) {
			float3 color = float3::RandomSphere(lcg, float3::zero, 0.5f).Abs();
			light lgt = {
				view.TransformPos(float3x3::RotateY(2.0f * pi * i / 8.0f).Transform(+float3::unitX)), 1,
				color, 0.0f,
			};
			lights.push_back(lgt);
		}
		ID3D11ShaderResourceView* light_srv = make_structured_buffer(dev, lights.data(), (int)lights.size(), sizeof(light));

		// Common setup
		ctx->VSSetConstantBuffers(0, 1, &matrix_cb);
		ctx->PSSetShaderResources(0, 1, &light_srv);

		std::vector<float4x4> instances;
		instances.resize(boxes.size());
		std::transform(boxes.begin(), boxes.end(), instances.begin(), [](float4x4 m) -> float4x4 {
			return m.Transposed();
		});
		{
			// Draw each bundle
			int instance_count;
			auto* mesh = &bar_box_mesh;
			auto* bundle = &instances;
			instance_count = (int)bundle->size();
			if (instance_count > 0) {
				std::vector<float4x4> data(bundle->begin(), bundle->end());
				ID3D11ShaderResourceView* instance_srv = make_structured_buffer(dev, data.data(), instance_count, sizeof(float4x4));
				draw_solid_mesh(dev, ctx, &g_solid_shader, mesh, instance_srv, instance_count);
				instance_srv->Release();
			}
		}

		// Common tear-down
		matrix_cb->Release();
		light_srv->Release();
	}
};

static scene_factory_t<waveform_bars_scene> g_fac_waveform_bars;