#pragma once

#include "shader_set.h"
#include "par_shapes.h"

#include "MathGeoLib.h"
#include <d3d11.h>

struct vertex {
	float4 position;
	float3 normal;   float _pad0;
	float2 texcoord; float2 _pad1;
};

struct vertex_mesh {
	ID3D11Buffer* ib;
	ID3D11Buffer* vb;
	ID3D11ShaderResourceView* vb_srv;
	uint32_t vertex_count;
};

inline vertex_mesh make_vertex_mesh(ID3D11Device* dev, int index_count, uint32_t const* indices, int vertex_count, vertex const* vertices) {
	HRESULT hr;
	vertex_mesh ret = {};
	{
		D3D11_BUFFER_DESC bd = {};
		bd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		bd.ByteWidth = vertex_count * sizeof(vertex);
		bd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
		bd.StructureByteStride = sizeof(vertex);
		bd.Usage = D3D11_USAGE_IMMUTABLE;
		D3D11_SUBRESOURCE_DATA srd = {};
		srd.pSysMem = vertices;
		hr = dev->CreateBuffer(&bd, &srd, &ret.vb);
		hr = dev->CreateShaderResourceView(ret.vb, nullptr, &ret.vb_srv);
	}
	{
		D3D11_BUFFER_DESC bd = {};
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.ByteWidth = index_count * sizeof(uint32_t);
		bd.Usage = D3D11_USAGE_IMMUTABLE;
		D3D11_SUBRESOURCE_DATA srd = {};
		srd.pSysMem = indices;
		hr = dev->CreateBuffer(&bd, &srd, &ret.ib);
	}
	ret.vertex_count = index_count;
	return ret;
}

inline vertex_mesh make_plane(ID3D11Device* dev, float width, float height) {
	float dx = width / 2.0f;
	float dz = height / 2.0f;
	float4 positions[] = {
		float4(-dx, 0.0f, -dz, 1.0f),
		float4(+dx, 0.0f, -dz, 1.0f),
		float4(-dx, 0.0f, +dz, 1.0f),
		float4(+dx, 0.0f, +dz, 1.0f),
	};
	float3 normals[] = {
		+float3::unitY,
	};
	float2 texcoords[] = {
		float2(0.0f, 0.0f),
		float2(1.0f, 0.0f),
		float2(0.0f, 1.0f),
		float2(1.0f, 1.0f),
	};

	vertex vertices[] = {
		{ positions[0], *normals, 0.0f, texcoords[0], float2::zero },
		{ positions[1], *normals, 0.0f, texcoords[1], float2::zero },
		{ positions[2], *normals, 0.0f, texcoords[2], float2::zero },
		{ positions[3], *normals, 0.0f, texcoords[3], float2::zero },
	};

	uint32_t indices[] = {
		0, 1, 2, 2, 1, 3,
		0, 2, 1, 1, 2, 3, // backside
	};
	return make_vertex_mesh(dev, _countof(indices), indices, _countof(vertices), vertices);
}

extern ID3D11BlendState* g_solid_blend;
extern ID3D11DepthStencilState* g_depth_writes;
extern ID3D11RasterizerState* g_solid_raster;

inline void draw_solid_mesh(ID3D11Device* dev, ID3D11DeviceContext* ctx, shader_set* shader, vertex_mesh* mesh, ID3D11ShaderResourceView* instance_srv, int instance_count) {
	ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	ctx->IASetInputLayout(shader->il);

	ctx->VSSetShader(shader->vs, nullptr, 0);
	
	ctx->PSSetShader(shader->ps, nullptr, 0);
	
	float4 blend_factors = float4(1.0f, 1.0f, 1.0f, 1.0f);
	ctx->OMSetBlendState(g_solid_blend, blend_factors.ptr(), D3D11_DEFAULT_SAMPLE_MASK);

	ctx->OMSetDepthStencilState(g_depth_writes, D3D11_DEFAULT_STENCIL_REFERENCE);

	ctx->RSSetState(g_solid_raster);

	ctx->IASetIndexBuffer(mesh->ib, DXGI_FORMAT_R32_UINT, 0);

	ID3D11ShaderResourceView* srvs[] = { mesh->vb_srv, instance_srv, };
	ctx->VSSetShaderResources(0, _countof(srvs), srvs);

	ctx->DrawIndexedInstanced(mesh->vertex_count, instance_count, 0, 0, 0);
}

extern vertex_mesh skybox_plane;
extern shader_set g_skybox_shader;
extern ID3D11DepthStencilState* g_skybox_depth;
extern ID3D11SamplerState* linear_sampler;

inline void draw_skybox(ID3D11Device* dev, ID3D11DeviceContext* ctx, ID3D11ShaderResourceView* cubemap_srv) {
	// assumes global matrices in b0

	ID3D11ShaderResourceView* skybox_instance_srv;
	{
		float4x4 mats[] = {
			float4x4::Translate(0.5f * +float3::unitX) * float4x4::RotateZ(+pi/2.0f),
			float4x4::Translate(0.5f * -float3::unitX) * float4x4::RotateZ(-pi/2.0f),
			float4x4::Translate(0.5f * +float3::unitY),
			float4x4::Translate(0.5f * -float3::unitY) * float4x4::RotateX(+pi),
			float4x4::Translate(0.5f * +float3::unitZ) * float4x4::RotateX(+pi/2.0f),
			float4x4::Translate(0.5f * -float3::unitZ) * float4x4::RotateX(-pi/2.0f),
		};
		for (int i = 0; i < _countof(mats); ++i) {
			mats[i] = mats[i].Transposed();
		}
		skybox_instance_srv = make_structured_buffer(dev, mats, _countof(mats), sizeof(float4x4));
	}

	ID3D11ShaderResourceView* vs_srvs[] = {
		skybox_plane.vb_srv, skybox_instance_srv,
	};

	ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	ctx->IASetIndexBuffer(skybox_plane.ib, DXGI_FORMAT_R32_UINT, 0);
	ctx->IASetInputLayout(g_skybox_shader.il);

	ctx->OMSetDepthStencilState(g_skybox_depth, D3D11_DEFAULT_STENCIL_REFERENCE);

	ctx->VSSetShader(g_skybox_shader.vs, nullptr, 0);
	ctx->VSSetShaderResources(0, _countof(vs_srvs), vs_srvs);
	ctx->PSSetShader(g_skybox_shader.ps, nullptr, 0);
	ctx->PSSetShaderResources(1, 1, &cubemap_srv);
	ctx->PSSetSamplers(0, 1, &linear_sampler);

	ctx->DrawIndexedInstanced(skybox_plane.vertex_count, 6, 0, 0, 0);

	skybox_instance_srv->Release();
}

inline vertex_mesh convert_par_shapes_mesh(ID3D11Device* dev, par_shapes_mesh* m) {
	std::vector<vertex> vertices(m->npoints);
	std::vector<uint32_t> indices(3 * m->ntriangles);
	for (int i = 0; i < m->npoints; ++i) {
		vertex vtx = {};
		vtx.position = float4(float3(m->points + 3 * i), 1.0f);
		vtx.normal = float3(m->normals + 3 * i);
		vtx.texcoord = m->tcoords ? float2(m->tcoords + 2 * i) : float2::zero;
		vertices[i] = vtx;
	}
	for (int i = 0; i < 3 * m->ntriangles; i += 3) {
		indices[i+0] = m->triangles[i+2];
		indices[i+1] = m->triangles[i+1];
		indices[i+2] = m->triangles[i+0];
	}
	return make_vertex_mesh(dev, (int)indices.size(), indices.data(), (int)vertices.size(), vertices.data());
}