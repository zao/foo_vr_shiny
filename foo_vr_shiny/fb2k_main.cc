#include <foobar2000.h>

#include <sstream>
#include <vector>

#pragma comment(lib,"shlwapi.lib")

DECLARE_COMPONENT_VERSION("Shiny VR Visualisation", "1.0", "zao")
VALIDATE_COMPONENT_FILENAME("foo_vr_shiny.dll")

struct standalone_communicator;
extern critical_section_static g_cs;
extern standalone_communicator* g_comm;

void notify_new_messages() {}

static struct job_object_holder {
	job_object_holder() {
		job = CreateJobObject(nullptr, nullptr);
		JOBOBJECT_EXTENDED_LIMIT_INFORMATION jeli = {};
		jeli.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
		SetInformationJobObject(job, JobObjectExtendedLimitInformation, &jeli, sizeof(jeli));
	}

	HANDLE job;
} job_object;

struct standalone_communicator {
	standalone_communicator() {
		pfc::string8 full_path = core_api::get_my_full_path();
		full_path.truncate_to_parent_path();
		full_path.add_filename("vr_shiny.exe -hosted");
		console::warning(full_path.c_str());
		SECURITY_ATTRIBUTES sa = {};
		sa.nLength = sizeof(SECURITY_ATTRIBUTES);
		sa.bInheritHandle = TRUE;
		HANDLE child_stdin_rd, child_stdout_wr;
		CreatePipe(&child_stdin_rd, &child_stdin_wr, &sa, 0);
		SetHandleInformation(child_stdin_wr, HANDLE_FLAG_INHERIT, 0);
		CreatePipe(&child_stdout_rd, &child_stdout_wr, &sa, 0);
		SetHandleInformation(child_stdout_rd, HANDLE_FLAG_INHERIT, 0);
		STARTUPINFOA si = {};
		si.cb = sizeof(STARTUPINFOA);
		si.hStdOutput = child_stdout_wr;
		si.hStdInput = child_stdin_rd;
		si.dwFlags |= STARTF_USESTDHANDLES;

		PROCESS_INFORMATION pi = {};
		char* cmd_line = strdup(full_path.c_str());
		CreateProcessA(nullptr, cmd_line, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &si, &pi);
		free(cmd_line);
		child_process = pi.hProcess;
		AssignProcessToJobObject(job_object.job, child_process);
		CloseHandle(pi.hThread);
		CloseHandle(child_stdin_rd);
		CloseHandle(child_stdout_wr);

		io_write_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		io_write_shutdown_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);

		io_read_thread = CreateThread(nullptr, 0, &io_read_proc, (void*)this, 0, 0);
		io_write_thread = CreateThread(nullptr, 0, &io_write_proc, (void*)this, 0, 0);
	}

	~standalone_communicator() {
		CloseHandle(child_stdout_rd);
		HANDLE hs[] = { io_read_thread, io_write_thread };
		WaitForMultipleObjects(_countof(hs), hs, TRUE, INFINITE);
		CloseHandle(io_read_thread);
		CloseHandle(io_write_thread);
		CloseHandle(io_write_event);
		CloseHandle(io_write_shutdown_event);
		CloseHandle(child_process);
	}

	static DWORD CALLBACK io_read_proc(void* param) {
		standalone_communicator* self = (standalone_communicator*)param;
		std::vector<char> input_buffer(1<<20);
		std::vector<std::string> new_messages;
		int num_valid_bytes = 0;
		while (1) {
			DWORD num_read = 0;
			BOOL read_success = ReadFile(self->child_stdout_rd, input_buffer.data() + num_valid_bytes, input_buffer.size() - num_valid_bytes, &num_read, nullptr);
			if (!read_success) {
				fb2k::inMainThread([self]{
					delete self;
					if (g_comm == self) {
						g_comm = nullptr;
					}
				});
				break;
			}
			int next_start = 0;
			int end = num_valid_bytes + num_read;
			for (int offset = num_valid_bytes; offset < end; ++offset) {
				if (input_buffer[offset] == '\n') {
					std::string s(input_buffer.data() + next_start, input_buffer.data() + offset);
					new_messages.push_back(s);
					next_start = offset + 1;
				}
			}
			if (next_start != 0) {
				num_valid_bytes = end - next_start;
				memmove(input_buffer.data(), input_buffer.data() + next_start, num_valid_bytes);
			}
			if (!new_messages.empty()) {
				self->io_cs.enter();
				self->io_read_queue.insert(self->io_read_queue.end(), new_messages.begin(), new_messages.end());
				new_messages.clear();
				notify_new_messages();
				self->io_cs.leave();
			}
		}
		return 0;
	}

	static DWORD CALLBACK io_write_proc(void* param) {
		standalone_communicator* self = (standalone_communicator*)param;
		while (1) {
			HANDLE hs[] = { self->child_process, self->io_write_event, self->io_write_shutdown_event };
			DWORD wait_res = WaitForMultipleObjects(_countof(hs), hs, FALSE, INFINITE);
			if (wait_res == WAIT_OBJECT_0) {
				return 0;
			}
			else if (wait_res == WAIT_OBJECT_0 + 1) {
				self->io_cs.enter();
				std::vector<std::string> local_queue;
				local_queue.swap(self->io_write_queue);
				self->io_cs.leave();
				int n = local_queue.size();
				for (int i = 0; i < n; ++i) {
					std::string const& s = local_queue[i];
					DWORD num_written = 0;
					BOOL write_success = WriteFile(self->child_stdin_wr, s.data(), s.size(), &num_written, nullptr);
					if (!write_success) {
						return 0;
					}
				}
			}
			else {
				CloseHandle(self->child_stdin_wr);
				return 0;
			}
		}
	}

	void shutdown() {
		io_cs.enter();
		SetEvent(io_write_shutdown_event);
		g_comm = nullptr;
		io_cs.leave();
	}

	void send_message(std::string const& msg) {
		io_cs.enter();
		io_write_queue.push_back(msg);
		SetEvent(io_write_event);
		io_cs.leave();
	}

	HANDLE child_process;
	critical_section_static io_cs;
	HANDLE io_read_thread, io_write_thread;
	HANDLE child_stdin_wr;
	HANDLE child_stdout_rd;

	HANDLE io_write_event;
	HANDLE io_write_shutdown_event;
	std::vector<std::string> io_write_queue;
	std::vector<std::string> io_read_queue;
};

critical_section_static g_cs;
standalone_communicator* g_comm = nullptr;
service_ptr_t<visualisation_stream_v2> g_vis;

UINT_PTR vis_tick_id;
void CALLBACK vis_sample_tick(HWND, UINT, UINT_PTR id, DWORD) {
	audio_chunk_impl chunk;
	double t = 0.0;
	int sample_count = 2048;
	if (g_vis->get_absolute_time(t) && g_vis->get_spectrum_absolute(chunk, t, sample_count * 2)) {
		std::ostringstream oss;
		oss << "spectrum " << sample_count;
		audio_sample* data = chunk.get_data();
		int n = chunk.get_sample_count();
		for (int i = 0; i < sample_count; ++i) {
			oss << ' ' << data[i];
		}
		oss << '\n';
		g_comm->send_message(oss.str());
	}
}

struct force_create_initquit : initquit {
	void on_init() override {
		g_comm = new standalone_communicator();
		static_api_ptr_t<visualisation_manager> vm;
		vm->create_stream(g_vis, visualisation_manager::KStreamFlagNewFFT);
		g_vis->set_channel_mode(visualisation_stream_v2::channel_mode_mono);
		vis_tick_id = SetTimer(nullptr, 0, 10, &vis_sample_tick);
	}

	void on_quit() override {
		KillTimer(nullptr, vis_tick_id);
		g_vis.release();
	}
};

static initquit_factory_t<force_create_initquit> g_factory_force_create;

struct vis_menu_command : mainmenu_commands {
	uint32_t get_command_count() override {
		return 1;
	}

	GUID get_command(uint32_t idx) override {
		// {442D4DB9-8F20-49E6-A4A8-4E78DE358EF0}
		static GUID const s_guid = {
			0x442d4db9, 0x8f20, 0x49e6, { 0xa4, 0xa8, 0x4e, 0x78, 0xde, 0x35, 0x8e, 0xf0 }
		};
		return s_guid;
	}

	void get_name(uint32_t idx, pfc::string_base& out) override {
		out = "Shiny VR";
	}

	bool get_description(uint32_t idx, pfc::string_base& out) override {
		out = "Shiny VR visualisation.";
		return true;
	}

	bool get_display(uint32_t idx, pfc::string_base& out, uint32_t& flags) override {
		auto* comm = InterlockedCompareExchangePointer((void**)&g_comm, nullptr, nullptr);
		flags = comm ? this->flag_checked : 0;
		get_name(idx, out);
		return true;
	}

	GUID get_parent() override {
		return mainmenu_groups::view_visualisations;
	}

	void execute(uint32_t idx, service_ptr) {
		g_cs.enter();
		if (g_comm) {
			console::warning("destroying comm");
			g_comm->shutdown();
			console::warning("destroyed comm");
		}
		else {
			console::warning("creating comm");
			g_comm = new standalone_communicator();
			console::warning("created comm");
		}
		g_cs.leave();
	}
};

static mainmenu_commands_factory_t<vis_menu_command> g_factory_vis_menu_commands;